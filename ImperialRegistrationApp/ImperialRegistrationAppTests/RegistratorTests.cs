﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImperialRegistrationApp.Tests
{
    [TestClass()]
    public class RegistratorTests
    {
        IRegistrator registrator;

        [TestInitialize]
        public void SetUp()
        {
            registrator = new Registrator();
        }

        #region argumentValidationTests
        // Null argument test
        // NullArgumentException is captured and returns false.
        [TestMethod]
        public void RegisterNullArgument()
        {
            Task<Boolean> result = registrator.Register(null);
            Assert.IsFalse(result.Result);
        }

        // Dto has null member
        // CustomException is captured and returns false.
        [TestMethod]
        public void RegisterNullDataStrings()
        {
            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            stringsDTO.dataStrings = null;

            Task<Boolean> result = registrator.Register(stringsDTO);
            Assert.IsFalse(result.Result);
        }

        // dataStrings is of size 0
        [TestMethod]
        public void RegisterDataStringsSize0()
        {
            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            List<String> dataStrings = new List<String>();
            stringsDTO.dataStrings = dataStrings;

            Task<Boolean> result = registrator.Register(stringsDTO);
            Assert.IsFalse(result.Result);
        }

        // dataStrings is of size 1
        [TestMethod]
        public void RegisterDataStringsSize1()
        {
            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            List<String> dataStrings = new List<String>
            {
                "Alex"
            };
            stringsDTO.dataStrings = dataStrings;

            Task<Boolean> result = registrator.Register(stringsDTO);
            Assert.IsFalse(result.Result);
        }

        // name is null
        [TestMethod]
        public void RegisterNullName()
        {
            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            List<String> dataStrings = new List<String>
            {
                null,
                "Earth"
            };
            stringsDTO.dataStrings = dataStrings;

            Task<Boolean> result = registrator.Register(stringsDTO);
            Assert.IsFalse(result.Result);
        }

        // planet is null
        [TestMethod]
        public void RegisterNullPlanet()
        {
            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            List<String> dataStrings = new List<String>
            {
                "Alex",
                null
            };
            stringsDTO.dataStrings = dataStrings;

            Task<Boolean> result = registrator.Register(stringsDTO);
            Assert.IsFalse(result.Result);
        }

        #endregion

        #region unicodeCharactersTests
        // Assert true test with basic latin unicode strings
        [TestMethod()]
        public async Task RegisterBasicLatinCharactersTest()
        {
            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            List<String> dataStrings = new List<String>
            {
                "! \" # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M N O",
                "P Q R S T U V W X Y Z [ \\ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~"
            };

            stringsDTO.dataStrings = dataStrings;

            Boolean actual = await Task.Run(() => registrator.Register(stringsDTO));

            Assert.IsTrue(actual);

        }

        // Assert true test with latin-1 Supplement unicode strings
        [TestMethod()]
        public async Task RegisterLatin1SupplementCharactersTest()
        {
            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            List<String> dataStrings = new List<String>
            {
                "¡ ¢ £ ¤ ¥ ¦ § ¨ © ª « ¬ ­ ® ¯ ° ± ² ³ ´ µ ¶ · ¸ ¹ º » ¼ ½ ¾ ¿ À Á Â Ã Ä Å Æ Ç È É Ê Ë Ì Í Î Ï",
                "Ð Ñ Ò Ó Ô Õ Ö × Ø Ù Ú Û Ü Ý Þ ß à á â ã ä å æ ç è é ê ë ì í î ï ð ñ ò ó ô õ ö ÷ ø ù ú û ü ý þ ÿ"
            };

            stringsDTO.dataStrings = dataStrings;

            Boolean actual = await Task.Run(() => registrator.Register(stringsDTO));

            Assert.IsTrue(actual);

        }

        // Assert true test with greek unicode strings
        [TestMethod()]
        public async Task RegisterGreekCharactersTest()
        {
            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            List<String> dataStrings = new List<String>
            {
                "ʹ ͵ ͺ ; ΄ ΅ Ά · Έ Ή Ί Ό Ύ Ώ ΐ Α Β Γ Δ Ε Ζ Η Θ Ι Κ Λ Μ Ν Ξ Ο Π Ρ Σ Τ Υ Φ Χ Ψ Ω Ϊ Ϋ ά έ ή ί ΰ",
                "α β γ δ ε ζ η θ ι κ λ μ ν ξ ο π ρ ς σ τ υ φ χ ψ ω ϊ ϋ ό ύ ώ ϐ ϑ ϒ ϓ ϔ ϕ ϖ Ϛ Ϝ Ϟ Ϡ Ϣ ϣ Ϥ ϥ Ϧ ϧ Ϩ ϩ Ϫ ϫ Ϭ ϭ Ϯ ϯ ϰ ϱ ϲ ϳ"
            };

            stringsDTO.dataStrings = dataStrings;

            Boolean actual = await Task.Run(() => registrator.Register(stringsDTO));

            Assert.IsTrue(actual);

        }
        #endregion

        #region integrationTest

        // Integration test
        [TestMethod()]
        public async Task RegisterIntegrationTestAsync()
        {
            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            List<String> dataStrings = new List<String>
            {
                "Alex",
                "Earth"
            };

            stringsDTO.dataStrings = dataStrings;

            Boolean actual = await Task.Run(() => registrator.Register(stringsDTO));

            Assert.IsTrue(actual);
        }

        #endregion

        #region threadingTests
        // multiple calls to the same service simultaneously
        [TestMethod]
        public async Task MultipleCallsTest()
        {

            ListOfStringsDTO stringsDTO = new ListOfStringsDTO();
            List<String> dataStrings = new List<String>
            {
                "someName",
                "somePlanet"
            };

            stringsDTO.dataStrings = dataStrings;

            Task<Boolean>[] tasks = new Task<Boolean>[10000];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = registrator.Register(stringsDTO);
            }

            Task.WaitAll(tasks);

            for (int i = 0; i < tasks.Length; i++)
            {
                Assert.IsTrue(tasks[i].Result);
            }

        }
        #endregion
    }
}