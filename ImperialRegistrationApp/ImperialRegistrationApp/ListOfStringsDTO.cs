﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ImperialRegistrationApp
{
    [DataContract]
    public class ListOfStringsDTO
    {
        [DataMember]
        public List<String> dataStrings { get; set; }
    }
}