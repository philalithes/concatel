﻿using System;

namespace ImperialRegistrationApp
{
    public class CustomException : Exception
    {
        // Cause property will be used to know why the exception was thrown.
        public string Cause { get; }

        public CustomException()
        {
        }

        public CustomException(string message) : base(message)
        {
        }

        public CustomException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // extended methods
        public CustomException(string message, string cause) : base(message)
        {
            this.Cause = cause;
        }

        public CustomException(string message, string cause, Exception innerException) : base(message, innerException)
        {
            this.Cause = cause;
        }

    }
}