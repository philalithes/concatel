﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ImperialRegistrationApp
{
    /// <summary>
    /// Implementation of Registrator service
    /// </summary>
    public class Registrator : IRegistrator
    {
        // logger
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // path and name to locate the archive
        string archiveFullName = string.Format("{0}\\archive.txt", AppDomain.CurrentDomain.BaseDirectory);

        /// <summary>
        /// Registers the name and planet with a date into a file named above.
        /// </summary>
        /// <param name="dataStringsDTO">The data with name and planet.</param>
        /// <returns>True if register goes well.</returns>
        public Task<Boolean> Register(ListOfStringsDTO dataStringsDTO)
        {
            try
            {
                log.Info("Starting Register method.");
                // Argument validation
                // if argument is null
                if (dataStringsDTO == null)
                {
                    throw new ArgumentNullException("dataStringsDTO");
                }

                // if dataStrings is null
                if (dataStringsDTO.dataStrings == null)
                {
                    throw new CustomException("", Constants.DATASTRINGS_NULL);
                }

                // if there are no strings
                if (dataStringsDTO.dataStrings.Count == 0)
                {
                    throw new CustomException("", Constants.DATASTRINGS_0_SIZE);
                }

                // if name is null
                if (dataStringsDTO.dataStrings[0] == null)
                {
                    throw new CustomException("", Constants.DATASTRINGS_0_NULL);
                }

                // if there is only 1 string
                if (dataStringsDTO.dataStrings.Count == 1)
                {
                    throw new CustomException("", Constants.DATASTRINGS_1_SIZE);
                }

                // if planet is null
                if (dataStringsDTO.dataStrings[1] == null)
                {
                    throw new CustomException("", Constants.DATASTRINGS_1_NULL);
                }

                log.Info("Argument for Register method has been validated.");

                return RegisterImpl(dataStringsDTO);
            }
            catch (CustomException ex)
            {
                // comparing same constant strings with == is ok
                if (ex.Cause == Constants.DATASTRINGS_NULL)
                {
                    log.Error("Null data was found.");
                }
                else if (ex.Cause == Constants.DATASTRINGS_0_SIZE)
                {
                    log.Error("The list of strings is empty.", ex);
                }
                else if (ex.Cause == Constants.DATASTRINGS_0_NULL)
                {
                    log.Error("Name field is null.", ex);
                }
                else if (ex.Cause == Constants.DATASTRINGS_1_SIZE)
                {
                    log.Error("The planet string is missing.", ex);
                }
                else if (ex.Cause == Constants.DATASTRINGS_1_NULL)
                {
                    log.Error("Planet field is null.", ex);
                }
            }
            catch (ArgumentNullException ex)
            {
                log.Error("Null data was found.", ex);
            }
            catch (Exception ex)
            {
                log.Error("An unexpected error ocurred.", ex);
            }
            log.Info("Reached end of method Register.");
            return Task.FromResult(false);
        }

        /// <summary>
        /// Logical implementation of Register method.
        /// </summary>
        /// <param name="dataStringsDTO">The validated data.</param>
        /// <returns>True if registration goes well.</returns>
        private async Task<Boolean> RegisterImpl(ListOfStringsDTO dataStringsDTO)
        {
            try
            {
                log.Info("Retrieving data...");
                // get required data
                DateTime time = DateTime.Now;
                string name = dataStringsDTO.dataStrings[0];
                string planet = dataStringsDTO.dataStrings[1];

                log.Debug("name is " + name);
                log.Debug("planet is " + planet);

                // build new string
                string line = string.Format("rebeld {0} on {1} at {2}", name, planet, time);

                log.Debug("line is " + line);

                log.Info("Writing in file...");
                // write in file
                using (StreamWriter archive = new StreamWriter(archiveFullName, true))
                {
                    await archive.WriteLineAsync(line);
                }
                log.Info(string.Format("File {0} has been written.", archiveFullName));
                return true;
            }
            catch (Exception ex)
            {
                log.Error("An error ocurred during registration.", ex);
                return false;
            }
        }
    }
}
