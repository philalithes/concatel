﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;

namespace ImperialRegistrationApp
{
    // Registrator service
    [ServiceContract]
    public interface IRegistrator
    {
        // Accepts a list of strings with name and planet, registers in file and return true if register goes fine.
        [OperationContract]
        Task<Boolean> Register(ListOfStringsDTO dataStrings);
    }
}
