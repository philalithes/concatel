﻿namespace ImperialRegistrationApp
{
    public static class Constants
    {
        // CustomException causes
        public static readonly string DATASTRINGS_NULL = "dataStrings is null.";
        public static readonly string DATASTRINGS_0_NULL = "dataStrings[0] is null.";
        public static readonly string DATASTRINGS_1_NULL = "dataStrings[1] is null.";
        public static readonly string DATASTRINGS_0_SIZE = "dataStrings has size 0";
        public static readonly string DATASTRINGS_1_SIZE = "dataStrings has size 1";
    }
}